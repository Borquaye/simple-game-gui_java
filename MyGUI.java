import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 * A class outlinging the GUI of a simple game
 * 
 * Daniel Borquaye
 */
public class MyGUI
{
    // instance variables - replace the example below with your own
    private int r=7;
    private int c=7;
    boolean turn = true;
    /**
     * Constructor for objects of class MyGUI
     */
    public MyGUI()
    {
        // initialise instance variables
        frame();
    }

    /**
     * Method to design and set up the main elements of the GUI
     */
    public void frame()
    {
        // put your code here
        JFrame frame = new JFrame("Connect 4");
        ImageIcon nuetral = new ImageIcon("brown.jpg");
        ImageIcon drop = new ImageIcon("drop.jpg");
        final JLabel position[][] = new JLabel [r][c];
        final JLabel red = new JLabel(new ImageIcon("red.jpg"));
        JLabel yellow = new JLabel(new ImageIcon(".jpg"));
        JButton pos[][] = new JButton[r][c];
        
        Container contentPane = frame.getContentPane();
        
        JMenuBar menubar = new JMenuBar();
        JMenu infoMenu = new JMenu("Info");
        JMenuItem aboutItem = new JMenuItem("About");
        frame.setJMenuBar(menubar);
        menubar.add(infoMenu);
        
        aboutItem.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            { 
                about(); 
            }
        });
        
        infoMenu.add(aboutItem);
        
        JPanel top = new JPanel(new GridLayout(1,7));
        contentPane.add(top, BorderLayout.NORTH);
        for (c=0; c<7; c++){
            pos[1][c]= new JButton(drop);
            top.add(pos[1][c]);
        }
        final JPanel grid = new JPanel(new GridLayout(7,7));
        contentPane.add(grid, BorderLayout.CENTER);
        for (r=0; r<7; r++){
            for (c=0; c<7; c++){
                position[r][c]= new JLabel(nuetral);
                grid.add(position[r][c]);
            }
        }
        pos[1][0].addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e){
                position[6][0].setIcon(new ImageIcon("red.jpg"));
            }
        });
        pos[1][1].addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e){
                position[6][1].setIcon(new ImageIcon("yellow.jpg"));
            }
        });
    
        frame.pack();
        frame.setVisible(true);
    }
    
     private void about()
    {
        JFrame fram = new JFrame("Connect 4");
        JLabel about = new JLabel("Connect 4 game - DB363");
        
        fram.setSize(400,400);
        fram.add(about);
        
        fram.pack();
        fram.setVisible(true);
        /*System.out.println("Connect 4 game - DB363");*/
    }
}
